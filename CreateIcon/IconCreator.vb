Imports IWshRuntimeLibrary

Public Class IconCreator
    Public Shared Function CreateShortCut(ByVal shortcutName As String, ByVal creationDir As String, ByVal targetFullpath As String, ByVal workingDir As String, ByVal iconFile As String, ByVal iconNumber As Integer) As Boolean
        Try
            If Not IO.Directory.Exists(creationDir) Then
                Dim retVal As DialogResult = MsgBox(creationDir & " does not exist. Do you wish to create it?", MsgBoxStyle.Question Or MsgBoxStyle.YesNo)
                If retVal = DialogResult.Yes Then
                    IO.Directory.CreateDirectory(creationDir)
                Else
                    Return False
                End If
            End If

            Dim wshshell As New WshShellClass

            Dim shortCut As IWshRuntimeLibrary.IWshShortcut
            shortCut = CType(wshshell.CreateShortcut(creationDir & "\" & shortcutName & ".lnk"), IWshRuntimeLibrary.IWshShortcut)
            shortCut.TargetPath = targetFullpath
            shortCut.WindowStyle = 1
            shortCut.Description = shortcutName
            shortCut.WorkingDirectory = workingDir
            shortCut.IconLocation = iconFile & ", " & iconNumber
            shortCut.Save()
            Return True
        Catch ex As System.Exception
            MsgBox(ex.ToString)
            Return False
        End Try
    End Function
End Class
